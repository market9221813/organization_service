package postgres

import (
	"organization_service/config"
	"organization_service/storage"
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Store struct {
	DB *pgxpool.Pool
	cfg config.Config
}

func New (ctx context.Context, cfg config.Config) (storage.IStorage, error){
	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)

	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil{
		fmt.Println("Error while parsing to pool config!",err.Error())
		return Store{}, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil{
		fmt.Println("Error while creating a new pool!",err.Error())
		return Store{},err
	}
 	
	return Store {
		DB: pool,
		cfg: cfg,
	}, nil
}

func (s Store) Close()  {
	s.DB.Close()
}

func (s Store) Branch() storage.IBranchStorage {
	return NewBranchRepo(s.DB)
}

func (s Store) BranchSellingPoint() storage.IBranchSellingPointStorage {
	return NewBranchSellingPointRepo(s.DB)
}

func (s Store) Staff() storage.IStaffStorage {
	return NewStaffRepo(s.DB)
}

func (s Store) Provider() storage.IProviderStorage {
	return NewProviderRepo(s.DB)
}