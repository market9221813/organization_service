package postgres

import (
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type branchRepo struct {
	DB *pgxpool.Pool
}

func NewBranchRepo (db *pgxpool.Pool) storage.IBranchStorage{
	return &branchRepo{
		DB: db,
	}
}

func (b *branchRepo) Create(ctx context.Context,createBranch *pb.CreateBranchRequest) (*pb.Branch, error) {
	branch := pb.Branch{}

	query := `INSERT INTO branches (id,name)
			values ($1, $2) 
		returning id, name,created_at::text `

	uid := uuid.New()

	err := b.DB.QueryRow(ctx, query, uid, createBranch.GetName()).Scan(
		&branch.Id,
		&branch.Name,
 		&branch.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating Branch!", err.Error())
		return &pb.Branch{},err
	}

	return &branch, nil
}

func (b *branchRepo) Get(ctx context.Context,pKey *pb.BranchPrimaryKey) (*pb.Branch, error) {
	branch := pb.Branch{}

	query := `SELECT id, name, created_at::text, updated_at::text from branches
				WHERE id = $1 AND deleted_at = 0 `
	err := b.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&branch.Id,
		&branch.Name,
 		&branch.CreatedAt,
		&branch.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Branch!", err.Error())
		return &pb.Branch{}, err
	}
	return &branch, nil
}

func (b *branchRepo) GetList(ctx context.Context, req *pb.GetBranchListRequest) (*pb.BranchesResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		branches = pb.BranchesResponse{}
	)

	countQuery := `SELECT count(1) from branches where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND name ilike '%%%s%%' `, search)
	}

	err := b.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of branches!", err.Error())
		return &pb.BranchesResponse{},err
	}

	query := `SELECT id, name,created_at::text, updated_at::text from branches 
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND name ilike '%%%s%%' `, search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := b.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.BranchesResponse{}, err
	}

	for rows.Next() {
		branch := pb.Branch{}

		err := rows.Scan(
			&branch.Id,
			&branch.Name,
 			&branch.CreatedAt,
			&branch.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning branches!",err.Error())
			return &pb.BranchesResponse{},err
		}

		branches.Branches = append(branches.Branches, &branch)
	}

	branches.Count = count

	return &branches, nil
}

func (b *branchRepo) Update(ctx context.Context, updBranch *pb.Branch) (*pb.Branch, error) {
	branch := pb.Branch{}

	query := `UPDATE branches SET name = $1, updated_at = NOW() 
			WHERE id = $2 AND deleted_at = 0 
		returning id, name, updated_at::text `
	err := b.DB.QueryRow(ctx, query, updBranch.GetName(),updBranch.GetId()).Scan(
		&branch.Id,
		&branch.Name,
 		&branch.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating branches!", err.Error())
		return &pb.Branch{}, err
	}

	return &branch, nil

}

func (b *branchRepo) Delete(ctx context.Context, pKey *pb.BranchPrimaryKey) (error)  {
	query := `UPDATE branches SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := b.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting Branch!",err.Error())
		return err
	}
	return nil
}

func (b *branchRepo) GetNameByID(ctx context.Context, req *pb.BranchPrimaryKey) (*pb.BranchNameResponse, error) {
	branchName := pb.BranchNameResponse{}
	query := `SELECT name from branches where id = $1 AND deleted_at = 0 `

	err := b.DB.QueryRow(ctx, query, req.Id).Scan(
		&branchName.Name,
	)
	if err != nil{
		fmt.Println("error while selecting branch name by id!",err.Error())
		return &pb.BranchNameResponse{},err
	}

	return &branchName, nil
}