package postgres

import (
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type staffRepo struct {
	DB *pgxpool.Pool
}

func NewStaffRepo (db *pgxpool.Pool) storage.IStaffStorage{
	return &staffRepo{
		DB: db,
	}
}

func (s *staffRepo) Create(ctx context.Context,createStaff *pb.CreateStaffRequest) (*pb.Staff, error) {
	staff := pb.Staff{}

	query := `INSERT INTO staff (id, first_name, last_name, phone, login, password, branch_selling_point_id, type)
			values ($1, $2, $3, $4, $5, $6, $7, $8) 
		returning id, first_name, last_name, phone,  
			branch_selling_point_id, type::text, created_at::text `

	uid := uuid.New()

	err := s.DB.QueryRow(ctx, query, 
		uid, 
		createStaff.GetFirstName(),
		createStaff.GetLastName(),
		createStaff.GetPhone(),
		createStaff.GetLogin(),
		createStaff.GetPassword(),
		createStaff.GetBranchSellingPointId(),
		createStaff.GetType(),
		).Scan(
		&staff.Id,
		&staff.FirstName,
 		&staff.LastName,
 		&staff.Phone,
 		&staff.BranchSellingPointId,
 		&staff.Type,
 		&staff.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating Staff!", err.Error())
		return &pb.Staff{},err
	}

	return &staff, nil
}

func (s *staffRepo) Get(ctx context.Context,pKey *pb.StaffPrimaryKey) (*pb.Staff, error) {
	staff := pb.Staff{}

	query := `SELECT id, first_name, last_name, phone, 
		branch_selling_point_id, type::text, created_at::text, updated_at::text from staff
				WHERE id = $1 AND deleted_at = 0 `
	err := s.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&staff.Id,
		&staff.FirstName,
 		&staff.LastName,
		&staff.Phone,
		&staff.BranchSellingPointId,
		&staff.Type,
		&staff.CreatedAt,
		&staff.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Staff!", err.Error())
		return &pb.Staff{}, err
	}
	return &staff, nil
}

func (s *staffRepo) GetList(ctx context.Context, req *pb.GetStaffListRequest) (*pb.StaffResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		staffs = pb.StaffResponse{}
	)

	countQuery := `SELECT count(1) from staff where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND first_name ilike '%%%s%%' OR last_name ilike '%%%s%%' OR phone ilike '%%%s%%' `, search,search,search)
	}

	err := s.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of Staff!", err.Error())
		return &pb.StaffResponse{},err
	}

	query := `SELECT id, first_name, last_name, phone, 
	branch_selling_point_id, type::text, created_at::text, updated_at::text from staff
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND first_name ilike '%%%s%%' OR last_name ilike '%%%s%%' OR phone ilike '%%%s%%' `, search,search,search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := s.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.StaffResponse{}, err
	}

	for rows.Next() {
		staff := pb.Staff{}

		err := rows.Scan(
			&staff.Id,
			&staff.FirstName,
			&staff.LastName,
			&staff.Phone,
			&staff.BranchSellingPointId,
			&staff.Type,
			&staff.CreatedAt,
			&staff.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning Staffs!",err.Error())
			return &pb.StaffResponse{},err
		}

		staffs.Staff = append(staffs.Staff, &staff)
	}

	staffs.Count = count

	return &staffs, nil
}

func (s *staffRepo) Update(ctx context.Context, updStaff *pb.Staff) (*pb.Staff, error) {
	staff := pb.Staff{}

	query := `UPDATE staff SET first_name = $1, last_name = $2, phone = $3, 
	branch_selling_point_id = $4, type = $5, updated_at = NOW() 
			WHERE id = $6 AND deleted_at = 0 
		returning id, first_name, last_name, phone,
		branch_selling_point_id, type::text, updated_at::text `
	err := s.DB.QueryRow(ctx, query, 
		updStaff.GetFirstName(),
		updStaff.GetLastName(),
		updStaff.GetPhone(),
		updStaff.GetBranchSellingPointId(),
		updStaff.GetType(),
		updStaff.GetId(),
		).Scan(
			&staff.Id,
			&staff.FirstName,
			&staff.LastName,
			&staff.Phone,
			&staff.BranchSellingPointId,
			&staff.Type,
 			&staff.UpdatedAt,
	    )
	if err != nil{
		fmt.Println("error while updating Staff!", err.Error())
		return &pb.Staff{}, err
	}

	return &staff, nil

}

func (s *staffRepo) Delete(ctx context.Context, pKey *pb.StaffPrimaryKey) (error)  {
	query := `UPDATE staff SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := s.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting Staff!",err.Error())
		return err
	}
	return nil
}

func (s *staffRepo) GetStaffByCredentials(ctx context.Context, req *pb.UserLoginRequest) (*pb.Staff, error) {
	staff := pb.Staff{}

	if err := s.DB.QueryRow(ctx, `select id, phone, login, password from staff where login = $1`, req.GetLogin()).Scan(
		&staff.Id,
		&staff.Phone,
		&staff.Login,
		&staff.Password,
	); err != nil {
		return nil, err
	}

	return &staff, nil
}