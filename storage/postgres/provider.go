package postgres

import (
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/storage"
	"context"
	"fmt"

 	"github.com/jackc/pgx/v5/pgxpool"
)

type providerRepo struct {
	DB *pgxpool.Pool
}

func NewProviderRepo (db *pgxpool.Pool) storage.IProviderStorage{
	return &providerRepo{
		DB: db,
	}
}

func (p *providerRepo) Create(ctx context.Context,createProvider *pb.CreateProviderRequest) (*pb.Provider, error) {
	provider := pb.Provider{}

	query := `INSERT INTO providers (id, name, phone, active)
			values ($1, $2, $3, $4) 
		returning id, name, phone, active::text, created_at::text `

	uid := "abcd123"

	err := p.DB.QueryRow(ctx, query, 
		uid, 
		createProvider.Name,
		createProvider.Phone,
		createProvider.Active,
		).Scan(
		&provider.Id,
		&provider.Name,
 		&provider.Phone,
 		&provider.Active,
 		&provider.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating Provider!", err.Error())
		return &pb.Provider{},err
	}

	return &provider, nil
}

func (p *providerRepo) Get(ctx context.Context,pKey *pb.ProviderPrimaryKey) (*pb.Provider, error) {
	provider := pb.Provider{}

	query := `SELECT id, name, phone, active::text, created_at::text, updated_at::text from providers
				WHERE id = $1 AND deleted_at = 0 `
	err := p.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&provider.Id,
		&provider.Name,
 		&provider.Phone,
		&provider.Active,
		&provider.CreatedAt,
		&provider.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Provider!", err.Error())
		return &pb.Provider{}, err
	}
	return &provider, nil
}

func (p *providerRepo) GetList(ctx context.Context, req *pb.GetProviderListRequest) (*pb.ProvidersResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		providers = pb.ProvidersResponse{}
	)

	countQuery := `SELECT count(1) from providers where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND name ilike '%%%s%%' OR phone ilike '%%%s%%' `, search,search)
	}

	err := p.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of Provider!", err.Error())
		return &pb.ProvidersResponse{},err
	}

	query := ` SELECT id, name, phone, active::text, created_at::text, updated_at::text from providers
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND name ilike '%%%s%%' OR phone ilike '%%%s%%' `, search,search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := p.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.ProvidersResponse{}, err
	}

	for rows.Next() {
		provider := pb.Provider{}

		err := rows.Scan(
			&provider.Id,
			&provider.Name,
			&provider.Phone,
			&provider.Active,
			&provider.CreatedAt,
			&provider.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning Provideres!",err.Error())
			return &pb.ProvidersResponse{},err
		}

		providers.Providers = append(providers.Providers, &provider)
	}

	providers.Count = count

	return &providers, nil
}

func (p *providerRepo) Update(ctx context.Context, updProvider *pb.Provider) (*pb.Provider, error) {
	provider := pb.Provider{}

	query := `UPDATE providers SET name = $1, phone = $2, active = $3, updated_at = NOW() 
			WHERE id = $4 AND deleted_at = 0 
		returning id, name, phone, active::text, updated_at::text `
	err := p.DB.QueryRow(ctx, query, 
		updProvider.GetName(),
		updProvider.GetPhone(),
		updProvider.GetActive(),
		updProvider.GetId(),
		).Scan(
			&provider.Id,
			&provider.Name,
			&provider.Phone,
			&provider.Active,
			&provider.UpdatedAt,
	    )
		fmt.Println("Provider:", &provider)
	if err != nil{
		fmt.Println("error while updating Provider!", err.Error())
		return &pb.Provider{}, err
	}

	return &provider, nil

}

func (p *providerRepo) Delete(ctx context.Context, pKey *pb.ProviderPrimaryKey) (error)  {
	query := `UPDATE providers SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := p.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting Provider!",err.Error())
		return err
	}
	return nil
}