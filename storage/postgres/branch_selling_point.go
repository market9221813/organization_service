package postgres

import (
	"context"
	"fmt"
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/storage"
	"strings"

	"github.com/jackc/pgx/v5/pgxpool"
)

type branchSellingPointRepo struct {
	DB *pgxpool.Pool
}

func NewBranchSellingPointRepo (db *pgxpool.Pool) storage.IBranchSellingPointStorage{
	return &branchSellingPointRepo{
		DB: db,
	}
}

func (b *branchSellingPointRepo) Create(ctx context.Context,createBranchSellingPoint *pb.CreateBranchSellingPointRequest) (*pb.BranchSellingPoint, error) {
	branchSellingPoint := pb.BranchSellingPoint{}

	query := `INSERT INTO branch_selling_points (id, address, phone, branch_id)
			values ($1, $2, $3, $4) 
		returning id, address, phone, branch_id, created_at::text `

	err := b.DB.QueryRow(ctx, query,
		createBranchSellingPoint.GetId(), 
		createBranchSellingPoint.GetAddress(),
		createBranchSellingPoint.GetPhone(),
		createBranchSellingPoint.GetBranchId(),
		).Scan(
		&branchSellingPoint.Id,
		&branchSellingPoint.Address,
		&branchSellingPoint.Phone,
		&branchSellingPoint.BranchId,
 		&branchSellingPoint.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating BranchSellingPoint!", err.Error())
		return &pb.BranchSellingPoint{},err
	}

	return &branchSellingPoint, nil
}

func (b *branchSellingPointRepo) Get(ctx context.Context,pKey *pb.BranchSellingPointPrimaryKey) (*pb.BranchSellingPoint, error) {
	branchSellingPoint := pb.BranchSellingPoint{}

	query := `SELECT id, address, phone, branch_id, created_at::text, updated_at::text from branch_selling_points
				WHERE id = $1 AND deleted_at = 0 `
	err := b.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&branchSellingPoint.Id,
		&branchSellingPoint.Address,
		&branchSellingPoint.Phone,
		&branchSellingPoint.BranchId,
 		&branchSellingPoint.CreatedAt,
		&branchSellingPoint.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting BranchSellingPoint!", err.Error())
		return &pb.BranchSellingPoint{}, err
	}
	return &branchSellingPoint, nil
}

func (b *branchSellingPointRepo) GetList(ctx context.Context, req *pb.GetBranchSellingPointListRequest) (*pb.BranchSellingPointsResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		branchSellingPoints = pb.BranchSellingPointsResponse{}
	)

	countQuery := `SELECT count(1) from branch_selling_points where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND phone ilike '%%%s%%' `, search)
	}

	err := b.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of branchSellingPointes!", err.Error())
		return &pb.BranchSellingPointsResponse{},err
	}

	query := `SELECT id, address, phone, branch_id, created_at::text, updated_at::text from branch_selling_points 
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND phone ilike '%%%s%%' `, search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := b.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.BranchSellingPointsResponse{}, err
	}

	for rows.Next() {
		branchSellingPoint := pb.BranchSellingPoint{}

		err := rows.Scan(
			&branchSellingPoint.Id,
			&branchSellingPoint.Address,
			&branchSellingPoint.Phone,
			&branchSellingPoint.BranchId,
 			&branchSellingPoint.CreatedAt,
			&branchSellingPoint.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning branchSellingPointes!",err.Error())
			return &pb.BranchSellingPointsResponse{},err
		}

		branchSellingPoints.BranchSellingPoints = append(branchSellingPoints.BranchSellingPoints, &branchSellingPoint)
	}

	branchSellingPoints.Count = count

	return &branchSellingPoints, nil
}

func (b *branchSellingPointRepo) Update(ctx context.Context, updBranchSellingPoint *pb.BranchSellingPoint) (*pb.BranchSellingPoint, error) {
	branchSellingPoint := pb.BranchSellingPoint{}

	query := `UPDATE branch_selling_points SET address = $1, phone = $2, branch_id = $3, updated_at = NOW() 
			WHERE id = $4 AND deleted_at = 0 
		returning id, address, phone, branch_id, updated_at::text `
	err := b.DB.QueryRow(ctx, query, 
		updBranchSellingPoint.GetAddress(),
		updBranchSellingPoint.GetPhone(),
		updBranchSellingPoint.GetBranchId(),
		updBranchSellingPoint.GetId(),
		).Scan(

		&branchSellingPoint.Id,
		&branchSellingPoint.Address,
		&branchSellingPoint.Phone,
		&branchSellingPoint.BranchId,
 		&branchSellingPoint.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating branchSellingPointes!", err.Error())
		return &pb.BranchSellingPoint{}, err
	}

	return &branchSellingPoint, nil

}

func (b *branchSellingPointRepo) Delete(ctx context.Context, pKey *pb.BranchSellingPointPrimaryKey) (error)  {
	query := `UPDATE branch_selling_points SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := b.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting BranchSellingPoint!",err.Error())
		return err
	}
	return nil
}

func (b *branchSellingPointRepo) GenerateBranchSellingPointID(ctx context.Context, branchID string, branchName *pb.BranchNameResponse) (string, error){
	words := strings.Fields(branchName.GetName())
 
	firstLetters := string(words[0][0]) + string(words[0][1])
 
	var count int
	query := `SELECT COUNT(*) FROM branch_selling_points WHERE branch_id = $1 `

	err := b.DB.QueryRow(ctx,query, branchID).Scan(&count)
	if err != nil {
		return "", err
	}	

 	newID := fmt.Sprintf("%s-%04d", strings.ToUpper(firstLetters), count+1)
	return newID, nil
}
