package storage

import (
	pb "organization_service/genproto/organization_service_protos"
	"context"
)

type IStorage interface {
	Close()
	Branch() IBranchStorage
	BranchSellingPoint() IBranchSellingPointStorage
	Staff() IStaffStorage
	Provider() IProviderStorage
}

type IBranchStorage interface {
	Create(context.Context, *pb.CreateBranchRequest) (*pb.Branch, error)
	Get(context.Context, *pb.BranchPrimaryKey) (*pb.Branch, error)
	GetList(context.Context, *pb.GetBranchListRequest) (*pb.BranchesResponse, error)
	Update(context.Context, *pb.Branch) (*pb.Branch, error)
	Delete(context.Context, *pb.BranchPrimaryKey) (error)
	GetNameByID(context.Context, *pb.BranchPrimaryKey) (*pb.BranchNameResponse, error)
}

type IBranchSellingPointStorage interface {
	Create(context.Context, *pb.CreateBranchSellingPointRequest) (*pb.BranchSellingPoint, error)
	Get(context.Context, *pb.BranchSellingPointPrimaryKey) (*pb.BranchSellingPoint, error)
	GetList(context.Context, *pb.GetBranchSellingPointListRequest) (*pb.BranchSellingPointsResponse, error)
	Update(context.Context, *pb.BranchSellingPoint) (*pb.BranchSellingPoint, error)
	Delete(context.Context, *pb.BranchSellingPointPrimaryKey) (error)
	GenerateBranchSellingPointID(context.Context, string, *pb.BranchNameResponse) (string, error)
}

type IStaffStorage interface {
	Create(context.Context, *pb.CreateStaffRequest) (*pb.Staff, error)
	Get(context.Context, *pb.StaffPrimaryKey) (*pb.Staff, error)
	GetList(context.Context, *pb.GetStaffListRequest) (*pb.StaffResponse, error)
	Update(context.Context, *pb.Staff) (*pb.Staff, error)
	Delete(context.Context, *pb.StaffPrimaryKey) (error)
	GetStaffByCredentials(context.Context, *pb.UserLoginRequest) (*pb.Staff, error)
}

type IProviderStorage interface {
	Create(context.Context, *pb.CreateProviderRequest) (*pb.Provider, error)
	Get(context.Context, *pb.ProviderPrimaryKey) (*pb.Provider, error)
	GetList(context.Context, *pb.GetProviderListRequest) (*pb.ProvidersResponse, error)
	Update(context.Context, *pb.Provider) (*pb.Provider, error)
	Delete(context.Context, *pb.ProviderPrimaryKey) (error)
}
