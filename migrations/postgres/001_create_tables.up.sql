CREATE TYPE staff_type_enum AS ENUM (
    'cashier',
    'admin',
    'storekeeper'
);

CREATE TYPE active_status_enum AS ENUM(
    'yes',
    'no'
);

CREATE TABLE IF NOT EXISTS branches (
    id uuid primary key,
    name varchar(50) unique,
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS branch_selling_points (
    id varchar(7) primary key,
    address text,
    phone varchar(13) unique,
    branch_id uuid references branches(id),
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0   
);

CREATE TABLE IF NOT EXISTS staff (
    id uuid primary key,
    first_name varchar(20),
    last_name varchar(20),
    phone varchar(13) unique,
    login varchar(128) unique,
    password varchar(128),
    branch_selling_point_id varchar(7) references branch_selling_points(id),
    type staff_type_enum,
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0   
);

CREATE TABLE IF NOT EXISTS providers (
    id uuid primary key,
    name varchar(50),
    phone varchar(13) unique,
    active active_status_enum,
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0   
);


