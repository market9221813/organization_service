package service

import (
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/grpc/client"
	"organization_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type staffService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedStaffServiceServer
}

func NewStaffService (storage storage.IStorage, services client.IServiceManager) *staffService{
	return &staffService{
		storage: storage,
		services: services,
	}
}

func (s *staffService) Create(ctx context.Context, req *pb.CreateStaffRequest) (*pb.Staff,error) {
	staff, err := s.storage.Staff().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating Staff!", err.Error())
		return &pb.Staff{},err
	}
	return staff, nil
}

func (s *staffService) Get(ctx context.Context, req *pb.StaffPrimaryKey) (*pb.Staff,error) {
	staff, err := s.storage.Staff().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting Staff!", err.Error())
		return &pb.Staff{},err
	}
	return staff, nil
}

func (s *staffService) GetList(ctx context.Context, req *pb.GetStaffListRequest) (*pb.StaffResponse,error) {
	staffs, err := s.storage.Staff().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting Staffs list!",err.Error())
		return &pb.StaffResponse{},err
	}
	return staffs, nil
}

func (s *staffService) Update(ctx context.Context, req *pb.Staff) (*pb.Staff, error) {
	staff, err := s.storage.Staff().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating Staff!",err.Error())
		return &pb.Staff{}, err
	}
	return staff, nil
}

func (s *staffService) Delete(ctx context.Context, req *pb.StaffPrimaryKey) (*emptypb.Empty,error) {
	err := s.storage.Staff().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting Staff!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}