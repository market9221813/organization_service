package service

import (
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/grpc/client"
	"organization_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type providerService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedProviderServiceServer
}

func NewProviderService (storage storage.IStorage, services client.IServiceManager) *providerService{
	return &providerService{
		storage: storage,
		services: services,
	}
}

func (p *providerService) Create(ctx context.Context, req *pb.CreateProviderRequest) (*pb.Provider,error) {
	provider, err := p.storage.Provider().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating provider!", err.Error())
		return &pb.Provider{},err
	}
	return provider, nil
}

func (p *providerService) Get(ctx context.Context, req *pb.ProviderPrimaryKey) (*pb.Provider,error) {
	provider, err := p.storage.Provider().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting provider!", err.Error())
		return &pb.Provider{},err
	}
	return provider, nil
}

func (p *providerService) GetList(ctx context.Context, req *pb.GetProviderListRequest) (*pb.ProvidersResponse,error) {
	providers, err := p.storage.Provider().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting providers list!",err.Error())
		return &pb.ProvidersResponse{},err
	}
	return providers, nil
}

func (p *providerService) Update(ctx context.Context, req *pb.Provider) (*pb.Provider, error) {
	provider, err := p.storage.Provider().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating provider!",err.Error())
		return &pb.Provider{}, err
	}
	return provider, nil
}

func (p *providerService) Delete(ctx context.Context, req *pb.ProviderPrimaryKey) (*emptypb.Empty,error) {
	err := p.storage.Provider().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting provider!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}