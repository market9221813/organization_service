package service

import (
	"context"
	"fmt"
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/grpc/client"
 	"organization_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type branchSellingPointSellingPointService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedBranchSellingPointServiceServer
}

func NewBranchSellingPointService (storage storage.IStorage, services client.IServiceManager) *branchSellingPointSellingPointService{
	return &branchSellingPointSellingPointService{
		storage: storage,
		services: services,
	}
}

func (b *branchSellingPointSellingPointService) Create(ctx context.Context, req *pb.CreateBranchSellingPointRequest) (*pb.BranchSellingPoint,error) {
	branchName, err := b.services.BranchService().GetNameByID(ctx, &pb.BranchPrimaryKey{Id: req.BranchId})
	if err != nil{
		fmt.Println("Error in service, while getting branch name by id!", err.Error())
		return &pb.BranchSellingPoint{},err
	}

	newID, err := b.storage.BranchSellingPoint().GenerateBranchSellingPointID(ctx,req.BranchId,branchName)
	if err != nil{
		fmt.Println("Error in service, while generating new ID for branch selling point!",err.Error())
		return &pb.BranchSellingPoint{},err
	}

	req.Id = newID

	branchSellingPoint, err := b.storage.BranchSellingPoint().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating BranchSellingPoint!", err.Error())
		return &pb.BranchSellingPoint{},err
	}
	return branchSellingPoint, nil
}

func (b *branchSellingPointSellingPointService) Get(ctx context.Context, req *pb.BranchSellingPointPrimaryKey) (*pb.BranchSellingPoint,error) {
	branchSellingPoint, err := b.storage.BranchSellingPoint().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting BranchSellingPoint!", err.Error())
		return &pb.BranchSellingPoint{},err
	}
	return branchSellingPoint, nil
}

func (b *branchSellingPointSellingPointService) GetList(ctx context.Context, req *pb.GetBranchSellingPointListRequest) (*pb.BranchSellingPointsResponse,error) {
	branchSellingPoints, err := b.storage.BranchSellingPoint().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting BranchSellingPointes list!",err.Error())
		return &pb.BranchSellingPointsResponse{},err
	}
	return branchSellingPoints, nil
}

func (b *branchSellingPointSellingPointService) Update(ctx context.Context, req *pb.BranchSellingPoint) (*pb.BranchSellingPoint, error) {
	branchSellingPoint, err := b.storage.BranchSellingPoint().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating BranchSellingPoint!",err.Error())
		return &pb.BranchSellingPoint{}, err
	}
	return branchSellingPoint, nil
}

func (b *branchSellingPointSellingPointService) Delete(ctx context.Context, req *pb.BranchSellingPointPrimaryKey) (*emptypb.Empty,error) {
	err := b.storage.BranchSellingPoint().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting BranchSellingPoint!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}

