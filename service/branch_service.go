package service

import (
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/grpc/client"
	"organization_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type branchService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedBranchServiceServer
}

func NewBranchService (storage storage.IStorage, services client.IServiceManager) *branchService{
	return &branchService{
		storage: storage,
		services: services,
	}
}

func (s *branchService) Create(ctx context.Context, req *pb.CreateBranchRequest) (*pb.Branch,error) {
	branch, err := s.storage.Branch().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating Branch!", err.Error())
		return &pb.Branch{},err
	}
	return branch, nil
}

func (b *branchService) Get(ctx context.Context, req *pb.BranchPrimaryKey) (*pb.Branch,error) {
	branch, err := b.storage.Branch().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting Branch!", err.Error())
		return &pb.Branch{},err
	}
	return branch, nil
}

func (b *branchService) GetList(ctx context.Context, req *pb.GetBranchListRequest) (*pb.BranchesResponse,error) {
	branches, err := b.storage.Branch().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting Branches list!",err.Error())
		return &pb.BranchesResponse{},err
	}
	return branches, nil
}

func (s *branchService) Update(ctx context.Context, req *pb.Branch) (*pb.Branch, error) {
	branch, err := s.storage.Branch().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating Branch!",err.Error())
		return &pb.Branch{}, err
	}
	return branch, nil
}

func (b *branchService) Delete(ctx context.Context, req *pb.BranchPrimaryKey) (*emptypb.Empty,error) {
	err := b.storage.Branch().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting Branch!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}

func (b *branchService) GetNameByID(ctx context.Context, req *pb.BranchPrimaryKey) (*pb.BranchNameResponse, error) {
	branchName, err := b.storage.Branch().GetNameByID(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting branch name by id!",err.Error())
		return &pb.BranchNameResponse{},err
	}

	return branchName, nil
}