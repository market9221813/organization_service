package service

import (
	"context"
 	"fmt"
 	pb "organization_service/genproto/organization_service_protos"
	"organization_service/grpc/client"
 	"organization_service/storage"
)

type authService struct {
	storage  storage.IStorage
	services client.IServiceManager
 	pb.UnimplementedAuthServiceServer
}

func NewAuthService(strg storage.IStorage, services client.IServiceManager) *authService {
	return &authService{
		storage:  strg,
		services: services,
 	}
}

func (a *authService) UserLogin(ctx context.Context, req *pb.UserLoginRequest) (*pb.UserLoginResponse, error) {
	staff, err := a.storage.Staff().GetStaffByCredentials(ctx, req)
	if err != nil {
		fmt.Println("Error while getting client's credentials!", err.Error())
 		return nil, err
	}

	return &pb.UserLoginResponse{
		Id:    staff.GetId(),
		Phone: staff.GetPhone(),
	}, nil
}

 
 
