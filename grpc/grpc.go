package grpc

import (
	pb "organization_service/genproto/organization_service_protos"
	"organization_service/grpc/client"
	"organization_service/service"
	"organization_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer (strg storage.IStorage, services client.IServiceManager)*grpc.Server{
	grpcServer := grpc.NewServer()
	
	pb.RegisterBranchServiceServer(grpcServer, service.NewBranchService(strg, services))
	pb.RegisterBranchSellingPointServiceServer(grpcServer, service.NewBranchSellingPointService(strg, services))
	pb.RegisterStaffServiceServer(grpcServer, service.NewStaffService(strg, services))
	pb.RegisterProviderServiceServer(grpcServer,service.NewProviderService(strg,services))
	pb.RegisterAuthServiceServer(grpcServer,service.NewAuthService(strg,services))
	reflection.Register(grpcServer)

	return grpcServer
}