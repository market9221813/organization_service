package client

import (
	"organization_service/config"
 	organization_service "organization_service/genproto/organization_service_protos"
	"fmt"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	BranchService() organization_service.BranchServiceClient
	BranchSellingPointService() organization_service.BranchSellingPointServiceClient
	StaffService() organization_service.StaffServiceClient
	ProviderService() organization_service.ProviderServiceClient
	AuthService() organization_service.AuthServiceClient
}

type grpcClients struct {
	branchService organization_service.BranchServiceClient
	branchSellingPointService organization_service.BranchSellingPointServiceClient
	staffService organization_service.StaffServiceClient
	providerService organization_service.ProviderServiceClient
	authService organization_service.AuthServiceClient
}

func NewGrpcClients (cfg config.Config)(IServiceManager, error){
	connOrganizationService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting organization_service!",err.Error())
		return nil, err
	}

	return &grpcClients{
		 branchService: organization_service.NewBranchServiceClient(connOrganizationService),
		 branchSellingPointService: organization_service.NewBranchSellingPointServiceClient(connOrganizationService),
		 staffService: organization_service.NewStaffServiceClient(connOrganizationService),
		 providerService: organization_service.NewProviderServiceClient(connOrganizationService),

		 authService: organization_service.NewAuthServiceClient(connOrganizationService),
	}, nil
}

func (g *grpcClients) BranchService() organization_service.BranchServiceClient{
	return g.branchService
}

func (g *grpcClients) BranchSellingPointService() organization_service.BranchSellingPointServiceClient {
	return g.branchSellingPointService
}

func (g *grpcClients) StaffService() organization_service.StaffServiceClient {
	return g.staffService
}

func (g *grpcClients) ProviderService() organization_service.ProviderServiceClient {
	return g.providerService
}

func (g *grpcClients) AuthService() organization_service.AuthServiceClient {
	return g.authService
}